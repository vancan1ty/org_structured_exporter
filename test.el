#!emacs --script
(princ "hi there " standard-output)

(with-temp-buffer
  (call-process "echo" nil t nil "bob")
  (princ (buffer-substring-no-properties 1 (line-end-position 0))))

