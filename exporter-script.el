#!/usr/bin/emacs --script
;(setq debug-on-error t)
;(setq debug-on-quit t)
(load-file (expand-file-name "exporter.el"  (file-name-directory load-file-name)))
(load-file (expand-file-name "berryutils.el"  (file-name-directory load-file-name)))
(setq *mbuffer* (find-file (elt command-line-args-left 0)))
(with-current-buffer *mbuffer*
  (berry-do-export))
;;  (with-temp-buffer
;;    (call-process "make" nil t nil "-k" "all")
;;    (princ (buffer-substring-no-properties 1 (line-end-position 0)))))

  ;; (goto-char (point-min))
  ;; (forward-word 10)
  ;; (princ (buffer-substring-no-properties (point-min) (point))
;  (princ (word-at-point)))
;; (word-at-point)
;; (setq *tvar* 10)
;; (while (> *tvar* 0)
;;   (princ (concat "hello " (elt command-line-args-left 0) "\n"))
;;   (setq *tvar* (- *tvar* 1)))

;; (setq *mbuffer* (find-file "berry-exporter-tests.el"))

