org_structured_exporter -- 12/21/2015
============================================

org_structured_exporter is a tool which builds on top of emacs' org-mode's excellent export functionality to support a new usage scenario -- exporting a folder tree of documents from a single org-mode document.

In brief, org_structured_exporter enables the following two workflows:

1. Website export.
	1. Export an entire website, complete with templating and theming, from a single org-mode file.
	2. Export a single webpage to its proper place within your website folder.
2. Standalone document export.
	1. Export a standalone html or pdf document containing a given section of an org-mode document. 

Read exporter.el to understand the full functionality.

exporter-script.el allows you to export your website from the command line using emacs batch mode.
