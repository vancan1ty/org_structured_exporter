(require 'cl)
(require 'cl-lib)
(require 'subr-x)

;(macroexpand '(print-transparent (+ 5 1) "please"))

;(-  (print-transparent (+ 5 1) "please work") 1)
;(-  (print-transparent (+ 5 1)) 1)
(defmacro print-transparent (form &optional comment)
  `(let ((oval ,form))
     (if ,comment
         (print (concat (prin1-to-string oval) " : " ,comment ))
         (print oval))
     oval
  ))

;http://stackoverflow.com/questions/1019778/how-to-check-if-a-string-is-empty-in-emacs-lisp
(defun empty-string-p (string)
  "Return true if the string is empty or nil. Expects string.  Different than string-empty-p in subr-x in that nil returns nil"
  (or (null string)
      (zerop (length (string-trim string)))))

(defun join (delimiter-str strs)
"join strings, but if a string is empty then discard it.  different than string-join in subr-x in that empty strings are discarded"
  (mapconcat 'identity (cl-remove-if 'empty-string-p strs) delimiter-str)
)

(defun berry--is-char-simple (char)
  (string-match "[A-Za-z0-9_/\\-]" (char-to-string char)))

(defun cleanify (string)
"returns a version of the string with all the 'suspicious' characters removed I.E. should be sufficient for naive pathname escaping

function is interactive because sometimes it is useful to call this that way.
"
(interactive "Mstring")
(let ((out (string-reverse (concat (cl-reduce
 (lambda (accum nxt)
   (if (berry--is-char-simple nxt)
                         (cons nxt accum)
                       accum))
 (string-to-list (replace-regexp-in-string " " "_" string))
 :initial-value '())))))
  (if (called-interactively-p 'any)
      (insert out)
    out)))

(defun segment-into-groups (ilist groupsize)
  "segment-into-groups (list 1 2 3 4 5 6 7 8) 3 -> ((1 2) (3 4 5) (6 7 8))"
  (do ((multiplier 0 (+ multiplier 1)) (accum ()) (minindex 0) (maxbound 0))
      ((>= maxbound (length ilist)) (reverse accum)) 
    (setq minindex (* multiplier groupsize))
    (setq maxbound (min (+ minindex groupsize) (length ilist)))
    (setf accum (cons (subseq ilist minindex maxbound) accum))))

(provide 'berryutils)
