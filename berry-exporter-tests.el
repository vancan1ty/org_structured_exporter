(defmacro test_org-test-wrapper (fn-to-run)
  `(with-current-buffer "test.org"
     (save-excursion
       (goto-char (point-min))
       ,fn-to-run
       )))

;below we test berry-stupid-obtain-is-folder
(ert-deftest is-folder-test-1 ()
  (should (equal
           (test_org-test-wrapper
            (progn
            (goto-char 0)
            (berry-stupid-obtain-is-folder)))
           t)))
(ert-deftest is-folder-test-2 ()
  (should (equal
           (test_org-test-wrapper
            (progn
            (goto-char 220)
            (berry-stupid-obtain-is-folder)))
           nil)))
(ert-deftest is-folder-test-3 ()
  (should (equal
           (test_org-test-wrapper
            (progn
            (goto-char 299)
            (berry-stupid-obtain-is-folder)))
           t)))
(ert-deftest is-folder-test-4 ()
  (should (equal
           (test_org-test-wrapper
            (progn
            (goto-char (point-max))
            (berry-stupid-obtain-is-folder)))
           nil))
  )

;below we test berry-stupid-obtain-header-hierarchy
(ert-deftest hierarchy-test-1 ()
  (should (equal
           (test_org-test-wrapper
            (progn
            (goto-char 0)
            (berry-stupid-obtain-header-hierarchy)))
           nil)))
(ert-deftest hierarchy-test-2 ()
  (should (equal
           (test_org-test-wrapper
            (progn
            (goto-char 259)
            (berry-stupid-obtain-header-hierarchy)))
           '((1 . "PUBLIC") (2 . "SOFTWARE STUFF"))
)))

;folder name creation test
(ert-deftest folder-hierarchy-t1 ()
    (should (equal
             (berry-create-folder-hierarchy-from-parents '((1 . "PUBLIC") (2 . "SOFTWARE STUFF")))
             "PUBLIC/SOFTWARE STUFF")))


