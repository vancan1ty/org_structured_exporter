;; features to add:
;; "blogpost-only" feature (maybe)
;; [x] rss feed (chose atom)
;; [x] highlight active tab
;; sticky links support - make possible for redirects when title of page changes
;; add indexes by tags and by date
;;CB note currently you have to open up everything in the buffer you want to export.
(require 'cl)
(require 'cl-lib)
(require 'subr-x)
(require 'berryutils "berryutils.el")
(setq lexical-binding nil)
(setq org-export-with-toc nil)
(setq debug-on-error t)
(require 'ox)

(setq *genroot* "../website_2015_content/generated_content")
(setq *blogroot* "../website_2015_content/generated_content/public/blog")
(setq number-of-posts-per-page 10)
(setq *tzstring* "EDT")
(setq *num-posts-in-atom-feed* 10)

;;CB -- this is bad
;;I use this global variable to keep track of things like DATEMODIFIED, TAGS, DATECREATED.
(setq *globalproperties* '())
(setq *blogpostlist* '())
(setq *tag-to-bplist-map* (make-hash-table :test 'equal))

(org-export-define-derived-backend 'bhtml 'html
  :options-alist '(
                   (:with-toc nil "toc" nil)
                   (:html-head-include-default-style nil "html-style" nil)
                   (:html-head-include-scripts nil "html-scripts" nil)
                   (:time-stamp-file nil))
  :translate-alist '(
                     (template . berry-html-page-template)
                     )
)

(org-export-define-derived-backend 'bhtml-inline-blog 'html
  :options-alist '(
                   (:with-toc nil "toc" nil)
                   (:html-head-include-default-style nil "html-style" nil)
                   (:html-head-include-scripts nil "html-scripts" nil)
                   (:time-stamp-file nil))
  :translate-alist '(
                     (template . berry-get-html-for-article)))


(defun berry-do-export ()
  (interactive)
  (with-current-buffer "data.org"
  (goto-char (point-min))
  (re-search-forward "^* PUBLIC")
  (beginning-of-line)
  (org-narrow-to-subtree)
  (berry-export-org-doc-to-subdocs *genroot* "data.org" t)
  (widen)
  ))

(defun berry-export-pdf-currentdir ()
  (interactive)
  (berry-export-section 'pdf t))

(defun berry-export-html-currentdir ()
  (interactive)
  (berry-export-section 'html t))

(defun berry-export-bhtml-currentdir ()
  (interactive)
  (berry-export-section 'bhtml nil "."))

(defun berry-export-html-outputdir(&optional rootfolder)
  (interactive)
  (if (eql rootfolder nil)
      (setq rootfolder *genroot*))
  (let* ((folder-hierarchy-str (print-transparent (berry-create-folder-hierarchy-from-parents
                                                   (berry-stupid-obtain-header-hierarchy))))
         (output-directory (print-transparent (downcase  (join "/" (list rootfolder folder-hierarchy-str))))))
      (berry-export-section 'html t output-directory)))

(defun berry-export-bhtml-outputdir(&optional rootfolder)
  (interactive)
  (if (eql rootfolder nil)
      (setq rootfolder *genroot*))
  (let* ((folder-hierarchy-str (print-transparent (berry-create-folder-hierarchy-from-parents
                                                   (berry-stupid-obtain-header-hierarchy))))
         (output-directory (print-transparent (downcase  (join "/" (list rootfolder folder-hierarchy-str))))))
    (berry-export-section 'bhtml nil output-directory)))

(defun berry-export-org-doc-to-subdocs (rootfolder &optional buffer makeblogposts)
  "does the exporting"
  (interactive "Mrootfolder")
  (message "STEP1")
  (setq *tag-to-bplist-map* (make-hash-table :test 'equal))
  (if (eq buffer nil)
      (setq buffer (current-buffer)))
  (if (file-directory-p rootfolder)
      (progn 
        (delete-directory rootfolder t)
        (message "DELETED %s" rootfolder)
        ))
  (make-directory rootfolder)
  (message "CREATED %s" rootfolder)
  (with-current-buffer buffer
    (save-excursion
      (show-all)
      (berry-goto-first-header) ;move point to first header
      (if (berry-stupid-obtain-is-folder) ;then invoke the next header finder
          (berry-get-point-to-next-header-to-export))
      (while (< (point) (point-max))
        (berry-export-bhtml-outputdir rootfolder)
        (berry-get-point-to-next-header-to-export))))
  (when makeblogposts
    (setq *globalproperties* ())
    (setup-blog-directory)
    (maphash
     (lambda (tag bplist)
       (generate-blog-pages tag bplist)
       (generate-atom-feed tag bplist))
     *tag-to-bplist-map*)
  ))

(defun berry-export-section (export-type &optional open-after output-directory)
  (message "exporting to %s" output-directory)
  (if (eql output-directory nil)
      (setq output-directory default-directory))
  (progn
    (setq output-directory (replace-regexp-in-string " " "_" output-directory))
    ;;try to create the folder, if it already exists its no problem, we just keep going.
    (if (not (file-directory-p output-directory))
        (make-directory output-directory)))
  (save-excursion 
    (end-of-line)
    (outline-previous-heading)
    (when (eql nil (org-get-heading t t))
      (print (format "Empty heading found when exporting  output-directory '%s'.  aborting." output-directory))
      (kill-emacs 1))
    (print (concat "heading: '" (org-get-heading t t) "'"))
    (let* (
           (headlineparts (print-transparent (split-string (org-get-heading t t) " " t)))
           (headline (print-transparent (join " " headlineparts)))
           (filename (print-transparent
                      (if output-directory
                          (concat (file-name-as-directory output-directory) (cleanify (downcase (join "_" headlineparts))))
                        (downcase (join "_" headlineparts)))))
           )
      (beginning-of-line)
      (condition-case nil
          (setq *globalproperties* (org-entry-properties (point)))
        (error nil))
      (setq *globalproperties* (cons (cons "linkpath" (berry-process-filename-to-linkpath filename)) *globalproperties*))
      (setq *globalproperties* (cons (cons "title" headline) *globalproperties*))
      (message "props %s" *globalproperties*)
      (next-line)
      (beginning-of-line)
      (set-mark (point))
      (forward-line -1)
      (berry-org-lbefore-next-heading-sog-level)
      (let ((thecontents (buffer-substring-no-properties (mark) (point)))
            (exp-buffer (get-buffer-create "berry-export-buffer.org")))
        (with-current-buffer exp-buffer
          (erase-buffer)
          (goto-char (point-min))
          (org-mode)
          (insert thecontents)
          (berry-do-if-header-item-not-found
           "^#\\+TITLE: " (lambda () (insert (concat "#+TITLE: " headline "\n"))))
          (berry-do-if-header-item-not-found
           "^#\\+OPTIONS: *toc.*" (lambda () (insert (concat "#+OPTIONS: toc:nil\n"))))
          (if (not (empty-string-p user-full-name))
              (berry-do-if-header-item-not-found
               "^#\\+AUTHOR: " (lambda () (insert (concat "#+AUTHOR: " user-full-name "\n")))))
          (org-element-parse-buffer)
          (let ((efilename
                 (if (eql export-type 'pdf)
                     (berry-org-latex-export-to-pdf filename)
                   (if (eql export-type 'bhtml)
                       (progn
                         (save-excursion
                           (when  (assoc "BLOGPOST" *globalproperties*)
                             (message "blogpost detected for %s " headline)
                             (org-export-to-buffer 'bhtml-inline-blog "*blogposttempbuffer*" nil nil nil nil '(:with-tags nil))
                             (setf (gethash "" *tag-to-bplist-map*) 
                                   (cons
                                    (list *globalproperties*
                                          (with-current-buffer "*blogposttempbuffer*"
                                            (buffer-substring-no-properties 1 (point-max))))
                                    (gethash "" *tag-to-bplist-map*)))
                             (let ((mtags (if (assoc "TAGS" *globalproperties*)
                                              (split-string (cdr (assoc "TAGS" *globalproperties*)) ":" t " *")
                                            nil)))
                               (cl-loop for tag in mtags do
                                        (setf (gethash tag *tag-to-bplist-map*) 
                                              (cons
                                               (list *globalproperties*
                                                     (with-current-buffer "*blogposttempbuffer*"
                                                       (buffer-substring-no-properties 1 (point-max))))
                                               (gethash tag *tag-to-bplist-map*))))
                               )))
                         (org-export-to-file export-type (print-transparent (concat filename ".html")) nil nil nil nil '(:with-tags nil))
                         )
                     (org-export-to-file export-type (print-transparent (concat filename "." (symbol-name export-type))) nil nil nil nil '(:with-tags nil))))))
            (if open-after
                (org-open-file efilename))))))))

;; based on org-html-template in ox-html.el
(defun berry-html-page-template (contents info &optional blog-mode blog-page-no older-page-no newer-page-no prefix)
  "Return complete document string after HTML conversion.
CONTENTS is the transcoded contents string.  INFO is a plist
holding various information."
  (message "berry-html-page-template blog-mode: %s, pn: %s, olderpn: %s, newerpn: %s" blog-mode blog-page-no older-page-no newer-page-no)
  (concat
   (berry-get-html-header info)
   "<body>\n"
   "<div id='wrapper'>\n"
   "  <div id='col1'>\n"
   (berry-get-html-top-of-page-and-nav info)
   "  </div>"
   "<div id='col2'>" 
  ;; CB the following not needed for now
   ;; (let ((link-up (org-trim (plist-get info :html-link-up)))
   ;;       (link-home (org-trim (plist-get info :html-link-home))))
   ;;   (unless (and (string= link-up "") (string= link-home ""))
   ;;     (format org-html-home/up-format
   ;;             (or link-up link-home)
   ;;             (or link-home link-up))))
   ;; Document contents.
   "<div id=\"content\">\n"
   (if blog-mode
       (concat
        (if (or (eql prefix nil) (eql prefix ""))
            (format "<div class=\"blogindicator\"><b>Berry Web Blog</b> -- <i>page %s</i></div>\n" blog-page-no)
          (format "<div class=\"blogindicator\"><b>%s -- Berry Web Blog</b> -- <i>page %s</i></div>\n" prefix blog-page-no))
        contents
        "\n<div id=\"blog_forwardback\" style=\"display: block\">\n"
        (when older-page-no
          (format "<div style=\"float: left\"><a href=\"%s%s.html\">Older Posts</a></div>\n" prefix older-page-no))
        (when newer-page-no
          (format "<div style=\"float: right\"><a href=\"%s%s.html\">Newer Posts</a></div>\n" prefix newer-page-no))
        "</div>\n"
        )
     (berry-get-html-for-article contents info t))
   "</div>"
   "</div>"
   "<div id='col3'>
"
   (if blog-mode
       (concat
        "<div class='c3offset'><b>Atom Feeds:</b></div>
"
        (generate-atom-feeds-listing-html prefix)
"<b>Tags:</b><br/>
"
        (generate-tags-listing-html *tag-to-bplist-map*))
     ""
     )
   "</div>
      <div style='clear: both'></div>
      <div id='footer'>
		&copy; 2017 Currell Berry. |
		<a href='/about.html'>About</a> | 
		<a href='/contact.html'>Contact Me</a>
                 | <a href='http://cvberry.com/gitweb?p=berry_website_verbatim.git;a=shortlog'>Site History</a>
      </div>
   </div>

   <script type='text/javascript' src='/org-js.js'></script>
   <script type='text/javascript'>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-37640437-1']);
    _gaq.push(['_trackPageview']);

    (function() {
       var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
       ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
 })();
    </script>
"
   ;; Closing document.
   "</body>\n</html>"))

(defun setup-blog-directory ()
  (if (file-directory-p *blogroot*)
      (progn 
        (delete-directory *blogroot* t)
        (message "DELETED %s" *blogroot*)
        ))
  (make-directory *blogroot*)
  (message "CREATED %s" *blogroot*)
)

(defun generate-blog-pages (prefix postlist)
  "loops through postlist and generate a set of blog post pages"
  (message "generating '%s' blog pages: %d entries" prefix (length postlist))
  (let* ((i 0)
         (posts
          (cl-mapcar
           (lambda (entry) (cadr entry))
           (sort-blogpostlist-by-date postlist)))
         (posts-grouped (segment-into-groups posts number-of-posts-per-page)))
    (message "done grouping posts.  now at export stage")
    (cl-loop for group in posts-grouped
             for i from 0 do
             (setq *globalproperties* (cons (cons "linkpath" (format "/blog/%s%s.html" prefix i)) *globalproperties*))
             (let ((modded-group
                    (loop for gi from 0 below (length group) collect
                          (let ((element (elt group gi)))
                            (if (>= (1+ gi) (length group))
                                element
                              (concat element "\n<hr class=\"postseparator\"/>\n"))))))
               (write-region
                (berry-html-page-template
                 (apply 'concat modded-group)
                 (berry-make-basic-info-plist "blog" "Currell Berry")
                 t
                 i
                 (if (< (+ i 1) (length posts-grouped))
                     (+ i 1)
                   nil)
                 (if (>= (- i 1) 0)
                     (- i 1)
                   nil)
                 prefix)
                nil
                (concat *blogroot* "/" prefix (int-to-string i) ".html"))))))

(defun sort-blogpostlist-by-date (inlist)
   (sort (copy-list inlist)
         (lambda (x y)
           (not (string-lessp (berry-date-component-from-blogpostlist-entry x)
                              (berry-date-component-from-blogpostlist-entry y))))))

;(apply 'concat (list "h" "a"))
;(setq test-table (make-hash-table :test 'equal))
;(setf (gethash "ab" test-table) (list "foo" "bar"))
;(setf (gethash "cd" test-table) (list "foo" "bar" "baz"))

;(generate-tags-listing-html test-table)

(defun generate-atom-feeds-listing-html (prefix)
  (concat
   "<ul>
<li><a href='atom.xml'>All Posts</a></li>
"
   (if (equal prefix "")
       ""
     (format "<li><a href='%s_atom.xml'>%s</a></li>
" prefix prefix))
   "</ul>
"))

(defun generate-tags-listing-html (tag-to-bplist-map)
  (let ((tags-and-counts
         (sort
          (cl-loop for tag being the hash-keys of tag-to-bplist-map
                   for postlist = (gethash tag tag-to-bplist-map) collect
                   (cons tag (length postlist)))
          (lambda (x y) (string-lessp (downcase (car x)) (downcase (car y)))))))
    (apply 'concat
           (cons "<ul>
"
                 (nconc (cl-loop for cursor in tags-and-counts collect
                                (format "<li><a href='%s0.html'>%s (%d)</a></li>
"                                  (car cursor) (if (equal "" (car cursor)) "All" (car cursor)) (cdr cursor)))
                       (cons "</ul>" nil))))))

;;(cons "<ul>"
;;      (nconc (list "a" "b")
;;            (cons "</ul" nil)))


(defun generate-atom-feed (prefix postlist)
  "loops through postlist and generate an atom feed"
  (message "generating '%s' atom feed: %d entries" prefix (min *num-posts-in-atom-feed* (length postlist)))
  (let* ((sortedbpairs (subseq
                        (sort-blogpostlist-by-date postlist)
                        0
                        (min *num-posts-in-atom-feed* (length postlist))))
         (atomized-posts (loop for entry in sortedbpairs collect (berry-atom-entry-wrap (car entry) (cadr entry)))))
    (write-region
     (berry-atom-feed-template
      (apply 'concat atomized-posts)
      (berry-make-basic-info-plist "blog" "Currell Berry"))
     nil
     (if (or (eql "" prefix) (eql nil prefix))
         (concat *blogroot* "/" "atom.xml")
       (concat *blogroot* "/" prefix "_atom.xml"))
       )))

(defun berry-atom-feed-template (contents info)
  "contents and info are the usual org-style variables passed into 
   exporting functions -- but we really only use contents here"
  (let ((export-time-str (format-time-string "%FT%T%z")))
    (concat "<?xml version=\"1.0\" encoding=\"utf-8\"?>

<feed xmlns=\"http://www.w3.org/2005/Atom\">

	<title>Berry Web Blog Feed</title>
	<subtitle>Computers and programming.</subtitle>
	<link href=\"http://cvberry.com/blog/atom.xml\" rel=\"self\" />
	<link href=\"http://cvberry.com/\" />
	<id>tag:cvberry.com/blog</id>
	<updated>" export-time-str "</updated>
"
        contents
"	
</feed>")))

;(berry-atom-feed-template "hello" nil)

;(assoc "title" (car (elt *blogpostlist* 0)))

(defun berry-atom-entry-wrap (props html)
  (concat  "
        <entry>
                <title>" (cdr (assoc "title" props)) "</title>
                <link href=\""(concat "http://cvberry.com" (cdr (assoc "linkpath" props))) "\" />
                <id>tag:"(cdr (assoc "linkpath" props)) "</id>
                <updated>" (subseq (cdr (or (assoc "DATEMODIFIED" props) (cons "DATEMODIFIED" "<1900-01-01>"))) 1 -1)  "</updated>
                <content type=\"xhtml\">
                        <div xmlns=\"http://www.w3.org/1999/xhtml\">
  "
                html
                "
                        </div>
                </content>
                <author>
                        <name>Currell Berry</name>
		</author>
	</entry>
"))


;(write-region "hello" nil "test1234.txt")
(search "/blog" "a/blog/foo")

(defun berry-get-navbar-as-string ()
  (let ((linkpath (cdr (or (assoc "linkpath" *globalproperties*) (cons "linkpath" "")))))
    (concat
     "<div id='navigation'>
         <ul>
           <li" (if (string-equal "/index.html" linkpath) " class=\"active\"" "") "> <a href='/index.html'> Home Page </a> </li>
           <li"  (if (eql (search "/portfolio" linkpath) 0) " class=\"active\"" "") "> <a href='/portfolio'> Portfolio </a> </li>
           <li"  (if (eql (search "/blog" linkpath) 0) " class=\"active\"" "") "> <a href='/blog/0.html'> Blog </a> </li>
           <li"  (if (eql (search "/software" linkpath) 0) " class=\"active\"" "")  "> <a href='/software/index.html'> Software </a> </li>
           <li" (if (eql (search "/cheat_sheets" linkpath) 0) " class=\"active\"" "") "> <a href='/cheat_sheets/index.html'> Cheat Sheets </a> </li>
     <!--      <li> <a href='/computermastery.html'> 'Mastering your Computer' </a> </li> -->
     <!--      <li> <a href='/liberalarts4years.html'> 'DIY Liberal Arts Education in 4 Years ' </a> </li> -->
           <li"  (if (eql (search "/tech_writings" linkpath) 0) " class=\"active\"" "")  "> <a href='/tech_writings/index.html'> Tech Writings </a> </li>
     <!--      <li> <a href='/essaysliving.html'> Essays on Living </a> </li> -->
           <li" (if (eql (search "/book_reviews" linkpath) 0) " class=\"active\"" "") "> <a href='/book_reviews/index.html'> Book Reviews </a> </li>
           <li" (if (eql (search "/links" linkpath) 0) " class=\"active\"" "")  "> <a href='/links.html'>Links</a> </li>
     <!--   <li> <a href='/downloads/cvberryresumegeneral_fall2015.pdf'> My Resume </a> </li> -->
           <!--<li> <a href='/search.html'> Search </a> </li>
           <li> <a href='/sitemap.html'> Site Index </a> </li>
           <li> <a href='/site_index.html'> Glossary </a> </li>-->
           <li id='siteMapLI'> <a href='/sitemap.html'> Site Map </a> </li>
         </ul>
       </div>"
     )))

;;(setq t-hash1 (make-hash-table))
;;(puthash 1 "one title" t-hash1)
;;(puthash 2 "two title" t-hash1)
;;t-hash1

;; in general, to get the title, for instance, we do
;; (plist-get info :title)
;; to extract data from the internal org-html representation, try the following
;; (org-export-data (plist-get info :title) info)
;; (taken from org-html--build-meta-info )
(defun berry-get-html-header (info)
  (concat 
   "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"
   \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
   <html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">
   <head>
   <meta name='viewport' content='width=device-width, initial-scale=1'>
"
   "   <title>"
   (if (string-or-null-p (plist-get info :title))
       (plist-get info :title) (org-export-data (plist-get info :title) info))
   "   </title>\n"

   "   <meta  http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />
   <meta  name=\"generator\" content=\"Org-mode\" />
   <meta  name=\"author\" content=\"Currell Berry\" />
   <link href=\"/blog/atom.xml\" type=\"application/atom+xml\" rel=\"alternate\" title=\"Berry Web ATOM Feed\" />
   <link rel='shortcut icon' href='images/favicon.ico' />
   <link rel='stylesheet' href='/org-style.css' type='text/css' media='screen' />
   <link rel='stylesheet' href='/styles.css' type='text/css' media='screen' />
   </head>\n"))

(defun berry-get-html-top-of-page-and-nav (info)
  (concat 
      "<div id='homelogo'>
          <h1>
         <a href='/index.html' >Berry Web</a>
          </h1>
       </div>\n"
          (berry-get-navbar-as-string)
          ))


(defun berry-get-html-for-article (contents info &optional full-page-article-mode)
  "unless you pass full-page-article-mode t the default is to include blog-post stuff"
  (concat
   (when (not full-page-article-mode)
     "\n<div class=\"blogpost\">\n")
   ;; Document title.
   (let ((title (plist-get info :title))
         (creationdateraw (assoc "DATECREATED" *globalproperties*)) 
         (modificationdateraw (assoc "DATEMODIFIED" *globalproperties*))
         (tags (if (assoc "TAGS" *globalproperties*) (split-string (cdr (assoc "TAGS" *globalproperties*)) ":" t " *")))
         )
     (concat (format "<h1 class=\"title %s\"><a href=\"%s\">%s</a></h1>\n"
                     (if full-page-article-mode " articletitle" " bptitle")
                     (cdr (or (assoc "linkpath" *globalproperties*) (cons "linkpath" "#")))
                     (org-export-data (or title "") info))
             "<div style='text-align: center'>"
             (concat  (if creationdateraw (concat "Posted: " (substring (cdr creationdateraw) 1 -1) ". "))
                      (if modificationdateraw (concat "Modified: " (substring (cdr modificationdateraw) 1 -1) ". "))
                      (if tags (concat "Tags: "
                                       (join ", " (mapcar
                                                   (lambda (tag) (format "<a href='/blog/%s0.html'>%s</a>" tag tag))
                                                   tags)) ".")))
             "</div>")
     )
   (if (assoc "REDIRECT" *globalproperties*)
       (format "<p>Automatically redirecting to <a href='%s'>new location</a> in <span id='redirectcountdown'>3</span> ...</p>
<script>
   var counter = 3;
   var checker = function() {
      if (counter > 0) {
        document.getElementById('redirectcountdown').innerHTML=counter;
        counter--;
        window.setTimeout(checker, 1000);
      } else {
        window.location='%s';
     }
   }
   checker();
</script>" (cdr (assoc "REDIRECT" *globalproperties*))  (cdr (assoc "REDIRECT" *globalproperties*))  )
     "")
   contents
   (when (not full-page-article-mode)
     "\n</div>\n")
   )
  )


(defun berry-make-basic-info-plist (title author)
  (list :title title :author author :html-doctype "xhtml"))

(defun berry-stupid-obtain-property (property)
  "precondition -- point is at the first character of the header you are about to export"
  (org-entry-get (point) property)
)

(defun berry-stupid-obtain-is-folder  ()
  "precondition -- point is at the first character of the header you are about to export"
  (let ((entry (berry-stupid-obtain-property "folder")))
    (if (equal entry "true")
                     t
                     nil)))

(defun berry-stupid-obtain-header-hierarchy ()    
  (interactive)
  (save-excursion
      (let ((olist ())
            (mparent (berry-get-parent-header)))
        (while mparent
          (push mparent olist)
          (setq mparent (berry-get-parent-header))
          )
        (print olist)
        olist
        )))

(defun berry-org-get-header-level ()
  "precondition -- point is at the first character of the header"
  (interactive)
  (save-excursion
   (search-forward-regexp "^\\(\\*+\\) \\(.*\\)$")
  (let* (
         (fullmatch (match-string-no-properties 0))
         (mlevelstr (match-string-no-properties 1))
         (level (length mlevelstr)))
    level)))
 
(defun berry-get-parent-header ()
  "precondition -- point is either at the start of the header you want to 
export or within the header's associated body, before any other headers appear."
                                        ;first, match the header where we're at
  (end-of-line)
  (outline-previous-heading)
  (search-forward-regexp "^\\(\\*+\\) \\(.*\\)$")
  (let* (
         (fullmatch (match-string-no-properties 0))
         (mlevelstr (match-string-no-properties 1))
         (level (length mlevelstr)))
    (if (<= level 1) 
        nil
        (let ((searchreg (concat "^"  (apply 'concat (make-list (- level 1) "\\*"))  " \\(.*\\)$")))
          (beginning-of-line)
          (search-backward-regexp searchreg)
          (let ((title (org-get-heading t t)))
            (cons (- level 1) title)
            )
          ))))

(defun berry-goto-first-header ()
    (goto-char (point-min))
    (search-forward-regexp "^\\(\*+\\) \\(.*\\)$")
    (beginning-of-line)
    (point)
)

(defun berry-get-point-to-next-header-to-export ()
  (let ((success nil)
        (iscurrentheaderfolder (berry-stupid-obtain-is-folder)))
    (if iscurrentheaderfolder 
        (while (not success)
          ;;then we need to obtain the next non-folder header of any type
          (let ((npoint (outline-next-heading)))
            (if (print-transparent npoint)
                (if (berry-stupid-obtain-is-folder) ; then keep going
                    nil
                    (progn
                      (setq success t)))
                (progn
                  (goto-char (point-max))
                  (setq success t)))))
        ;;then we need to find the next header at an equal or greater level
        ;;if this header is a folder header, we recall the function at that point
        
        ;;1. try to go forward at equal level.
        ;;2. if we can't go forward at equal level, 
        ;;keep trying to go up+forward until we successfully complete an operation
        ;;3. if we cannot go up at some point, we are at the end of our rope, and return
        ;;4. once we successfully go up+forward, then we recurse
        (while (not success)
                 (condition-case nil
                     (progn
                       (outline-forward-same-level 1)
                       (if (berry-stupid-obtain-is-folder) ;then we need to re-call the function at point
                           (progn
                             (berry-get-point-to-next-header-to-export) 
                             (setq success t))
                           (setq success t)))
                   (error ;;then we couldn't go straight forward.  do up/forward pairs till we are successful.
                      (condition-case whatswrong
                          (progn
                            (outline-up-heading 1))
                      (error
                       ;;then we can't go up.  set success=t and point=point-max
                       (progn
                         (goto-char (point-max))
                         (setq success t))))))))
    (point)))

(defun berry-create-folder-hierarchy-from-parents (parents)
  (mapconcat (lambda (x) (cdr x)) parents "/"))

(defun berry-do-if-header-item-not-found (item-re todo)
  (goto-char (point-min))
  (outline-next-heading)
  (let ((maxpoint (point)))
    (goto-char (point-min))
    (let ((tpoint (re-search-forward item-re maxpoint t)))
      (goto-char (point-min))
      (if (not tpoint)
          (funcall todo)))))


(defun berry-org-latex-export-to-pdf
  (outfile &optional async subtreep visible-only body-only ext-plist)
  "Export current buffer to LaTeX then process through to PDF.
   Derived from org-latex-export-to-pdf
   Return PDF file's name."
  (interactive)
    (org-export-to-file 'latex (print-transparent (concat outfile ".tex"))
      nil nil nil nil '()
      (lambda (file) (org-latex-compile (print-transparent file)))))
  
(defun berry-org-lbefore-next-heading-sog-level ()
  "Move forward to the line before the next heading at same level or greater level than this one."
  (interactive)
  (end-of-line)
  (outline-previous-heading)
  (beginning-of-line)
  (let ((level (berry-org-get-header-level)))
    (outline-next-heading)
    (while (and (< (point) (point-max)) (> (berry-org-get-header-level) level))
      (outline-next-heading)
      )
    (if (not (eql (point) (point-max)))
        (forward-line -1))
    (end-of-line)
    ))

(defun berry/export-rel-url (path desc format)
  (cl-case format
    (html (format "<a href=\"%s\">%s</a>" path (or desc path)))
    (latex (format "\href{%s}{%s}" path (or desc path)))
    (otherwise path)))

(defun berry/export-imgrel-url (path desc format)
  (cl-case format
    (html (format "<img src=\"%s\" alt =\"%s\"></img>" path (or desc path)))
    (latex (format "\includegraphics{%s}" path (or desc path)))
    (otherwise path)))

(org-add-link-type "rel" 'browse-url 'berry/export-rel-url)
(org-add-link-type "imgrel" 'browse-url 'berry/export-imgrel-url)


(defun berry-rss-timestr ()
  (concat (format-time-string "%a, %d %b %Y %T ") *tzstring*)
  )

(defun berry-next-pubdatestamp ()
  (interactive)
  (berry-xml-replace-next-tagcontents "pubDate" (berry-rss-timestr))
  )

(defun berry-xml-replace-next-tagcontents (tag contents)
  (interactive "stag:\nscontents:")
  (search-forward (concat "<" tag ">"))
  (let ((beg (point)))
    (search-forward (concat "</" tag ">"))
    (delete-region beg (point))
    (insert contents)
    (insert (concat "</" tag ">"))
    ))

(defun berry-date-component-from-blogpostlist-entry (entry)
  (cdr (or (assoc "DATECREATED" (car entry))
                          (cons "DATECREATED" "<2010-01-01>"))))

(defun berry-process-filename-to-linkpath (input)
  (concat
   (if (search "public" input)
       (substring input (+ (search "public" input) (length "public")))
     input)
   ".html"))

;;../website_2015_content/generated_content/public/tech_writings/notes/java_vs_lisp

;(with-current-buffer "essaysdoc.org"
;  (save-excursion
;    (berry-stupid-obtain-header-hierarchy)))


;(fset 'supercool
;   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([48 tab 118 36 121 tab 24 111 49 112 24 111 50 106] 0 "%d")) arg)))

;(with-current-buffer "test.org" (berry-get-point-to-next-header-to-export))
;(with-current-buffer "test.org" (berry-goto-first-header))

;(join " " (cl-subseq (split-string "** SOFTWARE STUFF" " " t) 1))

;(with-current-buffer "essaysdoc.org" (berry-stupid-obtain-header-hierarchy))


;;(elt *blogpostlist* 0)
;;(elt (sort-blogpostlist-by-date *blogpostlist*) 0)
;;(segment-into-groups (reverse (list 1 2 3 4 5 6 7 8)) 3)
