(require 'berryutils)

(ert-deftest join-test-1 ()
 (should (equal (join "/" '(" " "" "hello" "    " "" "there" " "))
                "hello/there")))
(ert-deftest join-test-2 ()
 (should (equal (join "/" '(" " ""))
                "")))

(ert-deftest cleanify-test-1 ()
 (should (equal (cleanify "hello there!")
                "hello_there")))
(ert-deftest cleanify-test-2 ()
 (should (equal (cleanify "ab CDe!@#$%&)(:::/z><,.")
                "ab_CDe/z")))
(ert-deftest cleanify-test-3 ()
 (should (equal (cleanify "this/is/a/path*^():-name")
                "this/is/a/pathname")))
(ert-deftest cleanify-test-4 ()
 (should (equal (cleanify "this\\is\\a\\path*^():-name")
                "this\\is\\a\\pathname")))


